App = Ember.Application.create();

App.Router.map(function() {
	this.route('home', {'path':'/'});
	this.route('employee', {'path':'/employee/:employee_id'});
});
//定义控制器home
App.HomeController = Ember.Controller.extend({
	'employeeInfo': ['a1', 'b2', 'c3']
});